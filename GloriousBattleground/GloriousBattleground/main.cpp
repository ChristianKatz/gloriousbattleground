#include <windows.h>
#include <tchar.h>
#include <cassert>
#include <iostream>
#include <string>
#include <conio.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 
#include <sstream>

using namespace std;

#define IDC_PLAY_BUTTON 100
#define ID_NEW 101
#define ID_LOAD 102
#define ID_SAVE 103
#define ID_CLOSE 104
#define ID_LISTBOX 105
#define ID_1_BUTTON 106
#define ID_2_BUTTON 107
#define ID_NEXT_BUTTON 108
#define ID_DAGGER_BUTTON 109
#define ID_SPEAR_BUTTON 110
#define ID_SWORD_BUTTON 111
#define ID_ARROWANDBOW_BUTTON 112
#define ID_RESTART_BUTTON 113
#define ID_FIGHT_BUTTON 114


int playerDamage;
int playerHealth;
int enemyDamage;
int enemyHealth = 100;

int enemyWeaponDamage;
int playerWeaponDamage;

int nextEnemy = 3;
LPCSTR enemyName;
bool nextFight = false;

int nextButtonCounter = 0;

HWND hwnd;
HWND staticText;
HWND playButton;
HWND nazanButton;
HWND mazanaButton;
HWND nextButton;
HWND daggerButton;
HWND spearButton;
HWND swordButton;
HWND arrowAndBowButton;
HWND restartButton;
HWND closeButton;
HWND fightButton;
HWND hList;

HINSTANCE hInstance = GetModuleHandle(0);

WNDCLASS wc;
MSG msg;

char buffer[2];

LPCSTR welcome = "Welcome to Glorious Battleground";
LPCSTR chooseCharacter = "Choose your Character";
LPCSTR Nazan = "You took Nazan. He has 100 Health";
LPCSTR Mazana = "You took Mazana. She has 100 Health";
LPCSTR Introduction = "Now you will sent in the arena to fight against the bloodthirsty enemy";

void WindowControll()
{
	hwnd = CreateWindow("Glorious Battleground", " Glorious Battleground ", WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, 800, 600, 0, 0, hInstance, 0);
	staticText = CreateWindow("Static", welcome, WS_TABSTOP | WS_VISIBLE | WS_CHILD, 320, 200, 260, 50, hwnd, 0, 0, 0);
	playButton = CreateWindow("button", "Play", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 350, 250, 200, 50, hwnd, (HMENU)IDC_PLAY_BUTTON, hInstance, 0);
	nazanButton = CreateWindow("button", "Nazan", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 300, 250, 100, 30, hwnd, (HMENU)ID_1_BUTTON, hInstance, 0);
	mazanaButton = CreateWindow("button", "Mazana", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 500, 250, 100, 30, hwnd, (HMENU)ID_2_BUTTON, hInstance, 0);
	nextButton = CreateWindow("button", "next", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 300, 250, 100, 30, hwnd, (HMENU)ID_NEXT_BUTTON, hInstance, 0);
	daggerButton = CreateWindow("button", "Dagger", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 500, 250, 100, 30, hwnd, (HMENU)ID_DAGGER_BUTTON, hInstance, 0);
	spearButton = CreateWindow("button", "Spear", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 350, 250, 100, 30, hwnd, (HMENU)ID_SPEAR_BUTTON, hInstance, 0);
	swordButton = CreateWindow("button", "Sword", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 200, 250, 100, 30, hwnd, (HMENU)ID_SWORD_BUTTON, hInstance, 0);
	arrowAndBowButton = CreateWindow("button", "Arrow and Bow", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 50, 250, 110, 30, hwnd, (HMENU)ID_ARROWANDBOW_BUTTON, hInstance, 0);
	restartButton = CreateWindow("button", "restart", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 250, 250, 100, 30, hwnd, (HMENU)ID_RESTART_BUTTON, hInstance, 0);
	closeButton = CreateWindow("button", "close", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 350, 250, 100, 30, hwnd, (HMENU)ID_CLOSE, hInstance, 0);
	fightButton = CreateWindow("button", "FIGHT", WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 350, 250, 100, 30, hwnd, (HMENU)ID_FIGHT_BUTTON, hInstance, 0);;

	ShowWindow(nazanButton, SW_HIDE);
	ShowWindow(mazanaButton, SW_HIDE);
	ShowWindow(nextButton, SW_HIDE);
	ShowWindow(daggerButton, SW_HIDE);
	ShowWindow(spearButton, SW_HIDE);
	ShowWindow(swordButton, SW_HIDE);
	ShowWindow(arrowAndBowButton, SW_HIDE);
	ShowWindow(restartButton, SW_HIDE);
	ShowWindow(closeButton, SW_HIDE);
	ShowWindow(fightButton, SW_HIDE);

}
void EnemeyWeaponChooser()
{
	srand(time(0));
	enemyWeaponDamage = (rand() % 5) + 14;
	enemyDamage = enemyWeaponDamage;
}
void NextRound()
{
	stringstream weapon;
	EnemeyWeaponChooser();
	weapon << "Your current enemy has got this round a weapon with " << enemyDamage << " Damage per hit" << endl;
	SetWindowText(staticText, weapon.str().c_str());
}
void DamageHealthCalculation()
{
	playerHealth -= enemyDamage;
	enemyHealth -= playerDamage;

	stringstream health;


	if (playerHealth <= 0 && enemyHealth >= 0)
	{
		playerHealth = 0;
		health << "You are Dead! It was a honor to fight by your side!" << endl;
		SetWindowText(staticText, health.str().c_str());
		ShowWindow(restartButton, SW_SHOW);
		ShowWindow(closeButton, SW_SHOW);
	}
	if (enemyHealth <= 0 && playerHealth >= 0)
	{
		enemyHealth = 0;
		health << "You have Won! Let's get to the next enemy!" << endl;
		SetWindowText(staticText, health.str().c_str());
		ShowWindow(nextButton, SW_SHOW);
		nextButtonCounter = 1;
		playerHealth = 100;

	}
	if (enemyHealth <= 0 && playerHealth <= 0)
	{
		health << "Cool, you both died! Neither of you is the winner" << endl;
		SetWindowText(staticText, health.str().c_str());
		ShowWindow(restartButton, SW_SHOW);
		ShowWindow(closeButton, SW_SHOW);
	}
	if (enemyHealth > 0 && playerHealth > 0)
	{
		health << "Argh, that hurts!. Your current Health is " << playerHealth << " and the enemy has still " << enemyHealth << " Health left" << endl;
		SetWindowText(staticText, health.str().c_str());
		ShowWindow(nextButton, SW_SHOW);
		nextButtonCounter = 2;
	}


}


void EnemyCreater()
{
	LPCSTR enemy[4] = { "Your enemy is the arena champion 'Tombros' with 100 Health", " Your enemy is the bloodthirsty killer 'Elites' with 100 Health", "Your enemy is the huge butcher 'Helio' with 100 Health", "Your enemy is the most evil soldier 'Srankos' with 100 Health" };
	LPCSTR finished;

	if (nextEnemy == 0)
	{
		finished = "You are a awesome Fighter!!!. Thank you for your great battles!!!";
		SetWindowText(staticText, finished);
		ShowWindow(closeButton, SW_SHOW);
		ShowWindow(restartButton, SW_SHOW);
	}
	if (nextEnemy > 0)
	{
		enemyName = enemy[nextEnemy];
		nextEnemy--;
		cout << enemyName << endl;
		SetWindowText(staticText, enemyName);
		enemyHealth = 100;
	}
}

LRESULT CALLBACK MessageHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	stringstream weaponChooser;
	switch (uMsg)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_COMMAND:
		if (LOWORD(wParam) == IDC_PLAY_BUTTON)
		{
			ShowWindow(playButton, SW_HIDE);
			SetWindowText(staticText, chooseCharacter);
			ShowWindow(nazanButton, SW_SHOW);
			ShowWindow(mazanaButton, SW_SHOW);
		}
		else if (LOWORD(wParam) == ID_1_BUTTON)
		{
			ShowWindow(nazanButton, SW_HIDE);
			ShowWindow(mazanaButton, SW_HIDE);

			SetWindowText(staticText, Nazan);
			playerHealth = 100;
			ShowWindow(nextButton, SW_SHOW);

		}
		else if (LOWORD(wParam) == ID_2_BUTTON)
		{
			ShowWindow(nazanButton, SW_HIDE);
			ShowWindow(mazanaButton, SW_HIDE);

			SetWindowText(staticText, Mazana);
			playerHealth = 100;
			ShowWindow(nextButton, SW_SHOW);
		}
		else if (LOWORD(wParam) == ID_RESTART_BUTTON)
		{
			ShowWindow(restartButton, SW_HIDE);
			ShowWindow(playButton, SW_SHOW);
			ShowWindow(closeButton, SW_HIDE);
			SetWindowText(staticText, welcome);
			nextButtonCounter = 0;
		}
		else if (LOWORD(wParam) == ID_NEXT_BUTTON)
		{

			SetWindowText(staticText, Introduction);

			if (LOWORD(wParam) == ID_NEXT_BUTTON && nextButtonCounter == 1)
			{
				EnemyCreater();
			}
			if (LOWORD(wParam) == ID_NEXT_BUTTON && nextButtonCounter == 2)
			{
				NextRound();
			}
			if (LOWORD(wParam) == ID_NEXT_BUTTON && nextButtonCounter == 3)
			{
				weaponChooser << "Choose your weapon!" << endl;
				SetWindowText(staticText, weaponChooser.str().c_str());

				ShowWindow(nextButton, SW_HIDE);
				ShowWindow(daggerButton, SW_SHOW);
				ShowWindow(spearButton, SW_SHOW);
				ShowWindow(swordButton, SW_SHOW);
				ShowWindow(arrowAndBowButton, SW_SHOW);
			}
			nextButtonCounter++;
			cout << nextButtonCounter << endl;
		}
		if (LOWORD(wParam) == ID_DAGGER_BUTTON)
		{
			srand(time(0));
			playerWeaponDamage = (rand() % 14) + 12;
			playerDamage = playerWeaponDamage;
			weaponChooser << "You have chosen the Dagger (" << playerDamage << " Damage per hit)" << endl;
			SetWindowText(staticText, weaponChooser.str().c_str());
			ShowWindow(arrowAndBowButton, SW_HIDE);
			ShowWindow(daggerButton, SW_HIDE);
			ShowWindow(spearButton, SW_HIDE);
			ShowWindow(swordButton, SW_HIDE);
			ShowWindow(fightButton, SW_SHOW);
		}
		if (LOWORD(wParam) == ID_SPEAR_BUTTON)
		{
			srand(time(0));
			playerWeaponDamage = (rand() % 15) + 9;
			playerDamage = playerWeaponDamage;
			weaponChooser << "You have chosen the Spear (" << playerDamage << "Damage per hit)" << endl;
			SetWindowText(staticText, weaponChooser.str().c_str());
			ShowWindow(arrowAndBowButton, SW_HIDE);
			ShowWindow(daggerButton, SW_HIDE);
			ShowWindow(spearButton, SW_HIDE);
			ShowWindow(swordButton, SW_HIDE);
			ShowWindow(fightButton, SW_SHOW);
		}
		if (LOWORD(wParam) == ID_SWORD_BUTTON)
		{
			srand(time(0));
			playerWeaponDamage = (rand() % 12) + 10;
			playerDamage = playerWeaponDamage;
			weaponChooser << "You have chosen the Sword (" << playerDamage << " Damage per hit)" << endl;
			SetWindowText(staticText, weaponChooser.str().c_str());
			ShowWindow(arrowAndBowButton, SW_HIDE);
			ShowWindow(daggerButton, SW_HIDE);
			ShowWindow(spearButton, SW_HIDE);
			ShowWindow(swordButton, SW_HIDE);
			ShowWindow(fightButton, SW_SHOW);
		}
		if (LOWORD(wParam) == ID_ARROWANDBOW_BUTTON)
		{
			srand(time(0));
			playerWeaponDamage = (rand() % 16) + 11;
			playerDamage = playerWeaponDamage;
			weaponChooser << "You have chosen the Arrow and Bow (" << playerDamage << "Damage per hit)" << endl;
			SetWindowText(staticText, weaponChooser.str().c_str());
			ShowWindow(arrowAndBowButton, SW_HIDE);
			ShowWindow(daggerButton, SW_HIDE);
			ShowWindow(spearButton, SW_HIDE);
			ShowWindow(swordButton, SW_HIDE);
			ShowWindow(fightButton, SW_SHOW);
		}
		if (LOWORD(wParam) == ID_FIGHT_BUTTON)
		{
			ShowWindow(fightButton, SW_HIDE);
			DamageHealthCalculation();
		}

		if (LOWORD(wParam) == ID_SAVE)
		{
			char filename[256];
			filename[0] = 0;
			OPENFILENAME saveDialog = {};
			saveDialog.lStructSize = sizeof(OPENFILENAME);
			saveDialog.lpstrFile = filename;
			saveDialog.nMaxFile = 256;
			saveDialog.lpstrFilter = "Text-Dateien\0*.txt;*.text\0Alle Dateien\0*.*";
			saveDialog.lpstrTitle = "Select file to save";
			saveDialog.Flags = OFN_OVERWRITEPROMPT;
			saveDialog.lpstrDefExt = ".txt";

			if (GetSaveFileName(&saveDialog) != 0)
			{
				cout << "Save file at:" << filename << endl;
			}
		}
		else if (LOWORD(wParam) == ID_CLOSE)
		{
			PostQuitMessage(0);
		}

		break;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void PopUpMenu()
{
	HMENU hMenu = CreateMenu();
	HMENU hFileMenu = CreateMenu();


	AppendMenu(hMenu, MF_POPUP, (UINT_PTR)hFileMenu, "File");
	AppendMenu(hFileMenu, MF_STRING, ID_NEW, "New");
	AppendMenu(hFileMenu, MF_STRING, ID_LOAD, "Load");
	AppendMenu(hFileMenu, MF_STRING, ID_SAVE, "Save");
	AppendMenu(hFileMenu, MF_STRING, ID_CLOSE, "Close");
	SetMenu(hwnd, hMenu);
}
void MainWindow()
{
	wc = {};
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = MessageHandler;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszClassName = "Glorious Battleground";
	assert(RegisterClass(&wc));

}

void RunningMainWindow()
{
	bool running = true;
	while (running)
	{
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				running = false;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

int main()
{
	MainWindow();
	WindowControll();
	PopUpMenu();
	RunningMainWindow();

	return 0;
}